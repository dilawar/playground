/***
 *    Description:  description
 *
 *        Created:  2020-02-20

 *         Author:  Dilawar Singh <dilawars@ncbs.res.in>
 *   Organization:  NCBS Bangalore
 *        License:  MIT License
 */

#include <iostream>

using namespace std;

int main(int argc, const char *argv[])
{
    bool a = false;
    double b = 1.0;
    cout << "Size of bool is " << sizeof(a) << endl;
    cout << "Size of double is " << sizeof(b) << endl;
    return 0;
}

