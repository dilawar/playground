__author__           = "Dilawar Singh"
__copyright__        = "Copyright 2019-, Dilawar Singh"
__maintainer__       = "Dilawar Singh"
__email__            = "dilawars@ncbs.res.in"

import HHTut
import netpyne as N

def main():
    N.sim.createSimulateAnalyze(netParams=HHTut.netParams, simConfig=HHTut.simConfig)


if __name__ == '__main__':
    main()

