#################################################################
## This program is part of 'MOOSE', the
## Messaging Object Oriented Simulation Environment.
##           Copyright (C) 2015 Upinder S. Bhalla. and NCBS
## It is made available under the terms of the
## GNU Lesser General Public License version 2.1
## See the file COPYING.LIB for the full notice.
##
## rxdSpineSize.py: Builds a cell with spines and a propagating reaction
## wave. Products diffuse into the spine and cause it to get bigger.
##################################################################
## Cdc42 activation profile (spatial and time) has to be accounted, 
## Tension modification
import math
import numpy as np
import matplotlib.pyplot as plt
import moose
print("using moose from %s", moose.__file__)
import sys
import rdesigneur as rd
import matplotlib
import moose.SBML
import os.path
import xml.etree.ElementTree as ET

PI = 3.141592653
ScalingForTesting = 10
RM = 1.0 / ScalingForTesting
RA = 1.0 * ScalingForTesting
CM = 0.01 * ScalingForTesting
runtime = 1740
loc1=str(sys.argv[6])
loc2=str(sys.argv[7])
loc3=str(sys.argv[8])
frameruntime = 1.0
diffConst = 5e-10
dendLen = 0.25e-6
comptLen = 1e-6
diffLen = 2.0e-7
dendDia = 1e-6
somaDia = 1e-6
concInit = 0.1    # 10 micromolar
spineSpacing = float(sys.argv[1])
spineSpacingDistrib = 1e-6
spineSize = 1.0
spineSizeDistrib = 0.5
spineAngle= np.pi / 2.0
spineAngleDistrib = 0.0
numDendSegments = 200


def writeXML(store,fileName):
    if os.path.isfile(fileName):
       tree = ET.parse(fileName)
       root = tree.getroot()
       #distr = ET.SubElement(root, 'calcium'+str(sys.argv[3])+"oth"+str(sys.argv[4]))
       for i in range(len(store)):
          avec = ET.SubElement(root, 'storeCalcium'+str(sys.argv[4])+"oth"+str(sys.argv[5]))
          avec.text = ''.join(str(j)+' ' for j in store[i])+'\n'
       tree.write(fileName) 
    else:
       root = ET.Element('Data')
       for i in range(len(store)):
          avec = ET.SubElement(root, 'storeCalcium'+str(sys.argv[4])+"oth"+str(sys.argv[5]))
          avec.text = ''.join(str(j)+' ' for j in store[i])+'\n'
       #times = ET.SubElement(root, 'times')
       #times.text = ''.join(str(j)+' ' for j in timeArr)+'\n'
       #xmaxFWHH = ET.SubElement(root, 'xmaxFWHH'+trialNum)
       #xmaxFWHH.text = ''.join(str(k)+' ' for k in maxFWHH)+'\n'
       tree = ET.ElementTree(root)
       tree.write(fileName)

def writeXML_td(time_d,fileName_td):
    if os.path.isfile(fileName_td):
       tree = ET.parse(fileName_td)
       root = tree.getroot()
       #distr = ET.SubElement(root, 'calcium'+str(sys.argv[3])+"oth"+str(sys.argv[4]))
       for i in range(len(time_d)):
          avec = ET.SubElement(root, 'time_d'+str(i)+str(fileName_td))
          avec.text = ''.join(str(j)+' ' for j in time_d[i])+'\n'
       tree.write(fileName_td) 
    else:
       root = ET.Element('Data')
       for i in range(len(time_d)):
          avec = ET.SubElement(root, 'time_d'+str(i)+str(fileName_td))
          avec.text = ''.join(str(j)+' ' for j in time_d[i])+'\n'
       #times = ET.SubElement(root, 'times')
       #times.text = ''.join(str(j)+' ' for j in timeArr)+'\n'
       #xmaxFWHH = ET.SubElement(root, 'xmaxFWHH'+trialNum)
       #xmaxFWHH.text = ''.join(str(k)+' ' for k in maxFWHH)+'\n'
       tree = ET.ElementTree(root)
       tree.write(fileName_td)

def computeTP(t,distS,SourceC):
    print('in computeTP', end= ' ')
    conv = moose.element( '/model/chem/dend/conv' )
    rec = moose.element( '/model/chem/dend/IRSpGr/rec' )
    w = len(conv.vec.n)
    h = w
    v2d = [[0 for x in range(w)] for y in range(h)]
    anC = []
    mm=moose.vec('/model/chem/dend/mesh')
    diffL = moose.element('/model/chem/dend').diffLength
    print(conv.vec[0].nInit, 'diff length ', diffL)
    list = [[distS,t]]
    SourceC = SourceC
    extraDiff = 0.05e-12
    for j in list:
       anC = []
       anC2 = [] 
       for i in range(len(conv.vec.n)):
      ##      anC.append(2.0*diffL*(SourceC/(np.sqrt(4*np.pi*extraDiff*j[1])))*np.exp(-(mm[i].Coordinates[0]**2)/(4*extraDiff*j[1])))
            for k in range(len(conv.vec.conc)):
        ##      anC2.append(4.0*diffL**2*(SourceC/(4*np.pi*j[1]*np.sqrt(extraDiff*extraDiff)))*np.exp(-(mm[i].Coordinates[0]**2)/(4*extraDiff*j[1])-(mm[k].Coordinates[0]**2)/(4*extraDiff*j[1])))
          ##    firstCell = diffL*(SourceC/(np.sqrt(4*np.pi*extraDiff*j[1])))*np.exp(-(mm[0].Coordinates[0]**2)/(4*extraDiff*j[1]))
              v2d[i][k] = diffL**2*(SourceC/(4*np.pi*j[1]*np.sqrt(extraDiff*extraDiff)))*np.exp(-(mm[i].Coordinates[0]**2)/(4*extraDiff*j[1])-(mm[k].Coordinates[0]**2)/(4*extraDiff*j[1]))
            ##  firstCell2 = diffL**2*(SourceC/(4*np.pi*j[1]*np.sqrt(extraDiff*extraDiff)))*np.exp(-(mm[0].Coordinates[0]**2)/(4*extraDiff*j[1])-(mm[0].Coordinates[0]**2)/(4*extraDiff*j[1]))
    ##print 'analaytical sum 1D is',sum(anC)-firstCell, 'max of anC is', max(anC)
    ##print 'analaytical sum 2D is',sum(anC2)-2*firstCell-firstCell2, 'max of anC2 is', max(anC2), firstCell, firstCell2
      #plt.plot(anC)
      #plt.plot(anC2)
    #raw_input()
    tempRec = []
    anP = True
    if anP == True:
        #plt.figure(2)
        #plt.title("alongX")
        for i in range(len(conv.vec.n)):
          tempRec.append(v2d[i][distS])
        #plt.plot(tempRec)
        #print 'sum conc at dendrite', sum(tempRec)
    return tempRec

def pert_int(posPer,SourceC):
      print('in pert_int', end=' ')
      timePer = int(sys.argv[9])
      distS = int(sys.argv[10])
      Ca = moose.element('/model/chem/dend/Ca')
      conv = moose.element('/model/chem/dend/conv')
      rec = moose.element('/model/chem/dend/IRSpGr/rec')
      Ca_vec = moose.vec('/model/chem/dend/Ca').conc
      tempRec = computeTP(timePer,distS,SourceC)
      moose.element('/model/chem/dend/IRSpGr/rec').vec[posPer].conc+=tempRec[0]
      for j in range(posPer+1,len(Ca_vec)):
           #rec.vec[j].conc=tempRec[j-posPer]
           moose.element('/model/chem/dend/IRSpGr/rec').vec[j].conc+=tempRec[j-posPer]
           if int(posPer*2-j)>=0:
               #rec.vec[posPer*2-j].conc=tempRec[j-posPer]
               moose.element('/model/chem/dend/IRSpGr/rec').vec[posPer*2-j].conc+=tempRec[j-posPer]
      moose.element('/model/chem/dend/IRSpGr/rec').conc=moose.element('/model/chem/dend/IRSpGr/rec').conc/2
      print('PERTURBED')

      return distS, timePer

def makeCellProto( name ):
    elec = moose.Neuron( '/library/' + name )
    ecompt = []
    #soma = rd.buildCompt( elec, 'soma', dx=somaDia, dia=somaDia, x=-somaDia, RM=RM, RA=RA, CM=CM )
    dend = rd.buildCompt( elec, 'dend', dx=dendLen, dia=dendDia, x=0, RM=RM, RA=RA, CM=CM )
    #moose.connect( soma, 'axial', dend, 'raxial' )
    prev=dend
    x=dendLen
    for i in range(numDendSegments):
      compt=rd.buildCompt(elec,'dend'+str(i),dx=dendLen, dia=dendDia, x=x, RM=RM, RA=RA, CM=CM)
      moose.connect(prev,'axial',compt,'raxial')
      prev=compt
      x=x+dendLen
    elec.buildSegmentTree()

def makeDendProto(name):
    dend=moose.Neuron('/library/dend')
    #prev=rd.buildCompt(dend,'soma',RM=RM,RA=RA,CM=CM,dia=0.3e-06,x=0,dx=comptLenBuff)
    prev=rd.buildCompt(dend,'soma',RM=RM,RA=RA,CM=CM,dia=2.0e-06,x=0,dx=comptLen)
    #x=comptLenBuff
    x=comptLen
    y=0.0
    comptDia=1e-06

    for i in range(numDendSegments):
      dx=comptLen
      dy=0
      #comptDia +=1.7e-08
      compt=rd.buildCompt(dend,'dend'+str(i),RM=RM,RA=RA,CM=CM,x=x,y=y,dx=dx,dy=dy,dia=comptDia)
      moose.connect(prev,'axial',compt,'raxial')
      prev=compt
      x+=dx
      y+=dy
      
    #compt=rd.buildCompt(dend,'dendL',RM=RM,RA=RA,CM=CM,x=x,y=y,dx=comptLenBuff,dy=dy,dia=comptDia)
    #moose.connect(prev,'axial',compt,'raxial')

#    return dend

def makeChemProto(name='hydra'):
        moose.Neutral('/library/')
        meshName='/library/'+name
        moose.SBML.mooseReadSBML('./c_m.xml',meshName)

        for f in moose.wildcardFind('/##[TYPE=Function]'):
            print(f.name, f.path, f.expr)

        Rad=moose.element('/library/hydra/dend/IRSpGr/Rad')

        ##shaftRad=moose.Pool('/library/hydra/spine/shaftRad')

        I_C=moose.element(meshName+'/dend/IRSpGr/IRSp53')
        Ca=moose.element(meshName+'/dend/Ca')
        rec=moose.Pool(meshName+'/dend/IRSpGr/rec')
        conv=moose.Pool(meshName+'/dend/conv')
        Cdc42=moose.element(meshName+'/dend/IRSpGr/Cdc42')
        Cdc42_m=moose.element(meshName+'/dend/IRSpGr/Cdc42_m')
        Cdc42_GDP=moose.element(meshName+'/dend/Cdc42_GDP')
        Rac_GTP=moose.element(meshName+'/dend/Rac_GTP')
        Rac_m=moose.element(meshName+'/dend/IRSpGr/Rac_m')
        Rac_GDP=moose.element(meshName+'/dend/Rac_GDP')
        Eps8=moose.element(meshName+'/dend/IRSpGr/Eps8')
        I_Cact=moose.element(meshName+'/dend/IRSpGr/IRSp53_a')
        I_M=moose.element(meshName+'/dend/IRSpGr/IRSp53_m')	
        Idimer=moose.element(meshName+'/dend/IRSpGr/IRSp53_dimer')
        recBAR=moose.Function(meshName+'/dend/IRSpGr/recBAR')
        gradBAR=moose.Function(meshName+'/dend/IRSpGr/gradBAR')
        ###Radfun=moose.element(meshName+'/dend/IRSpGr/Rad/func')
        Radfun=moose.Function(meshName+'/dend/IRSpGr/Radfun')
        ###shaftRadfun=moose.Function(meshName+'/spine/shaftRadfun')
        Ipool=moose.element(meshName+'/dend/IRSpGr/Ipool')
        sort=moose.element(meshName+'/dend/IRSpGr/sort')
        detach=moose.element(meshName+'/dend/IRSpGr/detach')
        curv_IRSp53=moose.element(meshName+'/dend/IRSpGr/curv_IRSp53')
        tube_IRSp53=moose.element(meshName+'/dend/IRSpGr/tube_IRSp53')
        to_tube_Reac=moose.element(meshName+'/dend/IRSpGr/to_tube_Reac')
        Cdc42_breac=moose.element(meshName+'/dend/Cdc42_GTP_GDP_Reac')
        Tiam_breac=moose.element(meshName+'/dend/Tiam_Rev_Reac')
        store_tube=moose.Pool(meshName+'/dend/IRSpGr/store_tube')
        IRSp53_temp=moose.element(meshName+'/dend/IRSpGr/IRSp53_temp')
        sigma=moose.Pool(meshName+'/dend/IRSpGr/sigma')
        Kv=moose.Pool(meshName+'/dend/IRSpGr/Kv')


        ##Spine_curv_IRSp53=moose.element(meshName+'/spine/curv_IRSp53')
        ##spine_vasp=moose.element(meshName+'/spine/VASP')

        dend_vasp=moose.element(meshName+'/dend/IRSpGr/VASP')

        ##irsp53_vasp=moose.element(meshName+'/spine/IRSp53_VASP')
        ##spine_gactin=moose.element(meshName+'/spine/Gactin')
        ##spine_factin=moose.element(meshName+'/spine/Factin')
        ##irsp53_vasp_fa=moose.element(meshName+'/spine/IRSp53_VASP_Fa')
        ##Psd_curv_IRSp53=moose.element(meshName+'/psd/curv_IRSp53')

        dend_gactin=moose.element(meshName+'/dend/IRSpGr/Gactin')
        dend_factin=moose.element(meshName+'/dend/IRSpGr/Factin')
        GF_Reac=moose.element(meshName+'/dend/IRSpGr/GF_Reac')
        sort_func=moose.element(meshName+'/dend/IRSpGr/sort_func')
        breacT_fun=moose.Function(meshName+'/dend/breacT_fun')
        breacC_fun=moose.Function(meshName+'/dend/breacC_fun')
        simpleFun=moose.Function(meshName+'/dend/IRSpGr/simpleFun')
        simpleFun1=moose.Function(meshName+'/dend/IRSpGr/simpleFun1')
        simpleFun2=moose.Function(meshName+'/dend/IRSpGr/simpleFun2')
        Kvfun=moose.Function(meshName+'/dend/IRSpGr/Kvfun')
        recFun=moose.Function(meshName+'/dend/IRSpGr/recFun')
        simpleFun2=moose.Function(meshName+'/dend/IRSpGr/simpleFun2')
        simpleFun3=moose.Function(meshName+'/dend/IRSpGr/simpleFun3')
        ##enzn=moose.element(meshName+'/dend/IRSpGr/curv/Enz')
        ##enzncplx=moose.element(meshName+'/dend/IRSpGr/curv/Enz/Enz2_cplx')
        mod_sort=moose.element(meshName+'/dend/IRSpGr/mod_sort')
        mod_detach=moose.element(meshName+'/dend/IRSpGr/mod_detach')
        to_tube_Reac=moose.element(meshName+'/dend/IRSpGr/to_tube_Reac')
        ##tube_reac=moose.element(meshName+'/dend/IRSpGr/tube_reac')
        ##enzn2=moose.element(meshName+'/dend/IRSpGr/sort/Enz')
        ##enzn3=moose.element(meshName+'/dend/IRSpGr/detach/Enz')
        #  Radfun.x.num=5

        conv_fun=moose.Function(meshName+'/dend/IRSpGr/conv_fun')
        #  conv_fun.x.num=2
        conv_fun.expr="1*x0*(x1-1.89191155)+1.89191155"
        #  Kvfun.x.num=0
        #Kvfun.expr="1.5*exp(-((x0-0.2)*(x0-0.2))/(1.5*0.2*0.2))"
        Kvfun.expr="1.0"
        print(Kvfun.expr)
        #moose.connect(sigma,'nOut',Kvfun.x[0],'input')
        moose.connect(Kvfun,'valueOut',Kv,'setN')
         

        simpleFun.expr="1.0*x0"
        simpleFun1.expr="1.0*x0"
        simpleFun2.expr="0.1*x0"
        simpleFun3.expr="1.0*x0"
        breacT_fun.expr="5*x0"
        breacC_fun.expr="5*x0"

        moose.connect(curv_IRSp53,'nOut',simpleFun2.x[0],'input')
        moose.connect(simpleFun2,'valueOut',GF_Reac,'setNumKf')
       
        moose.connect(sort_func,'nOut',simpleFun.x[0],'input')
        moose.connect(detach,'nOut',simpleFun1.x[0],'input')
        moose.connect(store_tube,'nOut',simpleFun3.x[0],'input')

        recFun.expr="0.0*x0+(exp(45*54*(("+str(sys.argv[14])+"/(x0*1e9))-(1/(2.0*(x0*1e9)*(x0*1e9))))))"
        Radfun.expr="sqrt(((45+12)*4*1e-18)/(2*(x2-0.08*(ln((((1.0*x0+1.0*x1)/(2*3.14*(0.45*0.05)))*1e-6*54))))))*((3*"+str(float(sys.argv[2]))+"*x3*(x0+1.0*x1)^2)/(1.+3*"+str(float(sys.argv[2]))+"*x3*(x0+1.0*x1)^2))" ##+  sqrt((55*4*1e-18)/(2*0.03))*(x0<0.1)"


        mod_sort.Kf=0.
        mod_sort.Kb=0.
        mod_detach.Kf=0.
        mod_detach.Kb=0.
        


        moose.connect(curv_IRSp53,'nOut',Radfun.x[0],'input')
        moose.connect(I_M,'nOut',Radfun.x[1],'input')
        #moose.connect(tube_IRSp53,'nOut',Radfun.x[2],'input')
        moose.connect(sigma,'nOut',Radfun.x[2],'input')
        moose.connect(Kv,'nOut',Radfun.x[3],'input')
        moose.connect(Radfun,'valueOut',Rad,'setN')

        moose.connect(rec,'nOut',conv_fun.x[0],'input')
        moose.connect(conv,'nOut',conv_fun.x[1],'input')
        moose.connect(conv_fun,'valueOut',Ca,'setN')
        
        ##moose.connect(Spine_curv_IRSp53,'nOut',shaftRadfun.x[0],'input')
        ##moose.connect(shaftRadfun,'valueOut',shaftRad,'setN')

        print(Radfun.expr)
        moose.connect(simpleFun,'valueOut',mod_sort,'setNumKf')
        moose.connect(simpleFun1,'valueOut',mod_detach,'setNumKf')
        
     
        moose.connect(Rad,'nOut',recFun.x[0],'input')
        moose.connect(recFun,'valueOut',Ipool,'setN')

### Ipool is further copied to sort to participate in the reaction tube_reac or enz (model version 2.5.3)

        I_C.concInit=0.0012
        file_molWt='prot_wt.xml'
        den=6.0*3.14*np.cbrt(3.0/(4.0*3.14*0.73*6.02*1e23))
        num=1e3*1e6*1.38*1e-23
        co=num/den
        

        # Diffconsts are set here
        if int(sys.argv[3])==1:
          print(f"[INFO ] Setting diffusion constants")
          for prot in moose.wildcardFind(meshName+'/dend/#[ISA=Pool]'):
            mol_name=ET.parse(file_molWt).find(str(prot.name))
            molWt=mol_name.text.split()
            if float(molWt[1]) > 0:
                diffConst = co*(300.0/(200.0*np.cbrt(float(molWt[1])*1e3)))*1e-4
            moose.element(prot).diffConst = diffConst
            print('D', prot.name,  moose.element(prot).diffConst)
          for prot in moose.wildcardFind(meshName+'/dend/CaMKII_gr/#[ISA=Pool]'):
            mol_name=ET.parse(file_molWt).find(str(prot.name))
            molWt=mol_name.text.split()
            moose.element(prot).diffConst=co*(300/(200.0*np.cbrt(float(molWt[1])*1e3)))*1e-4
            print('D', prot.name, moose.element(prot).diffConst)
          for prot in moose.wildcardFind(meshName+'/dend/Ras_gr/#[ISA=Pool]'):
            mol_name=ET.parse(file_molWt).find(str(prot.name))
            molWt=mol_name.text.split()
            moose.element(prot).diffConst=co*(300/(200.0*np.cbrt(float(molWt[1])*1e3)))*1e-4
            print('D', prot.name, moose.element(prot).diffConst)
          Ca.diffConst=float(sys.argv[4])
          print('D', Ca.name, Ca.diffConst)

        print('DIFFUSION FOR IRSp53 GROUP')
        I_C.diffConst=co*(300/(8.0*np.cbrt(120.0*1e3)))*1e-4
        print(I_C.diffConst, end=', ')
        I_Cact.diffConst=co*(300/(8.0*np.cbrt(208.0*1e3)))*1e-4
        print(I_Cact.diffConst, end=', ')
        Eps8.diffConst=co*(300/(200.0*np.cbrt(92.0*1e3)))*1e-4
        print(Eps8.diffConst, end=', ')
        Cdc42.diffConst=co*(300/(8.0*np.cbrt(22.0*1e3)))*1e-4
        Cdc42_m.diffConst=co*(300/(200.0*np.cbrt(22.0*1e3)))*1e-4
        Cdc42_GDP.diffConst=co*(300/(8.0*np.cbrt(22.0*1e3)))*1e-4
        print(Cdc42.diffConst, end=', ')
        Rac_GTP.diffConst=co*(300/(8.0*np.cbrt(22.0*1e3)))*1e-4
        Rac_m.diffConst=co*(300/(200.0*np.cbrt(22.0*1e3)))*1e-4
        Rac_GDP.diffConst=co*(300/(8.0*np.cbrt(22.0*1e3)))*1e-4

        I_M.diffConst=1.0*co*(300/(200.0*np.cbrt(392.0*1e3)))*1e-4
        print(I_M.diffConst, end=', ')
        Idimer.diffConst=co*(300/(8.0*np.cbrt(120.0*1e3)))*1e-4
        print(Idimer.diffConst, end=', ')
        curv_IRSp53.diffConst=float(sys.argv[17])

        ##Spine_curv_IRSp53.diffConst=1e-13
        ##Psd_curv_IRSp53.diffConst=1e-13
        ##irsp53_vasp.diffConst=1e-13
        ##irsp53_vasp_fa.diffConst=1e-13
        ##spine_gactin.diffConst=1e-13
        ##spine_factin.diffConst=1e-13
        
        conv.diffConst=0.0
        rec.diffConst=0.0


        dend_gactin.diffConst=1e-13
        dend_vasp.diffConst=1e-13

        return meshName

def makeModel():
    moose.Neutral( '/library' )
    makeCellProto( 'cellProto' )
    #makeDendProto('cellProto')
    makeChemProto()
    #moose.SBML.readSBML('/home/vinu/Documents/Biophysics/new_mod/detailed_models/oct01_volScale/mv1.xml','/model')
    rdes = rd.rdesigneur( 
            useGssa = False,
            turnOffElec = False,
            chemDt = 0.0025,
            diffDt = 0.0025,
            chemPlotDt = 0.1,
            combineSegments = False,
            stealCellFromLibrary = True,
            diffusionLength = 50e-9,
            cellProto = [['cellProto', 'elec' ]] ,
            chanProto = [['make_NMDA()','NMDA'],
                         ['make_Ca_conc()', 'Ca_conc'],
                        ],
            #cellProto = [['ballAndStick', 'soma', 10e-6, 1e-6, 2e-6, 9e-6, 1]],
            #spineProto = [["makeActiveSpine()","spine"]],
            #spineDistrib = [["spine","dend#",str(spineSpacing),"-1.0e-6","0.001","0.0"]],
            chemProto = [['hydra','chem']] ,
            chemDistrib = [[ "chem", "#", "install", "1" ]],
            chanDistrib = [['NMDA', 'dend#', 'Gbar', '1' ],
                           ['Ca_conc', 'dend#', 'tau', '0.033'],
                          ],
            stimList=[
                      #['dend#','1.','NMDA','periodicsyn',str(sys.argv[13])+'*(t>0 && t<60)'],
                      ['dend#','1.','NMDA','periodicsyn',str(sys.argv[13])+'*(t>0 && t<60)+'+str(sys.argv[13])+'*(t>'+str(sys.argv[15])+' && t<('+str(sys.argv[15])+'+60))'],
                      #['dend#','1.','NMDA','periodicsyn',str(sys.argv[13])+'*(t>0 && t<60)'],
                      #['dend#','1.','NMDA','periodicsyn',str(sys.argv[13])+'*(t>0 && t<60)'],
                     ],
            adaptorList = [
                  [ 'Ca_conc', 'Ca', 'dend/conv', 'conc', 0.00008, 20  ]
                          ],

            ##adaptorList = [
            ##    [ 'spine/IRSp53_VASP_Fa', 'n', 'spine', 'headVolume', 0.0, 10.0e-18 ],
            ##    [ 'spine/IRSp53_VASP', 'n', 'spine', 'psdArea', 0.0, 1e-12 ],
            ##    [ 'spine/shaftRad', 'n', 'spine', 'shaftDiameter', 0.0, 100.0 ],
            ##    [ 'spine/shaftRad', 'n', 'spine', 'shaftLength', 0.0, 100.0 ]
            ##    ],
        )
    moose.seed(100)
    #rd.rstim('dend250','1.','NMDA','periodicsyn','0.5*(t>0 && t<3)')
    rdes.buildModel( '/model' )
    
    moose.element('/model/chem/dend/IRSpGr/Rad').nInit=30e-9
   ## moose.element('/model/chem/dend/IRSpGr/stoich').allowNegative=1

def makePlot( name, srcVec, field ):
    tab = moose.Table2('/graphs/' + name + 'Tab', len( srcVec ) ).vec
    for i in zip(srcVec, tab):
        moose.connect(i[1], 'requestOut', i[0], field)
    return tab


def displayPlots():
    fig = plt.figure( figsize = ( 20, 20 ) )
    #plt.suptitle( 'Demo: Spine diameter changes as per z conc, y just diffuses. Pool concs are reported unless otherwise indicated.', fontsize = 18)
    wild = moose.wildcardFind( '/graphs/#[0]' )
    j = 1
    for x in wild:
        plt.subplot( len(wild)/3, 3, j )
        j += 1
        tab = moose.vec( x )
        for i in range( len( tab ) ):
            v = np.array( tab[i].vector )
            plt.plot( v )
            #plt.plot( v, label=x.name + " " + str( i ) )
        #plt.title( x.name )
        plt.ylabel( x.name[:-3], fontsize = 16 )
        #pylab.legend()
        #pylab.figure()

def radCalc():
    model=moose.Neutral('/model')
    hello=moose.PyRun('/model/hello')
    hello.initString="""print('initialising')
count=0
print('initial count'),print(count)"""
    hello.runString="""print('running')
count+=1
print('count'),print(count)"""
    moose.useClock(0,hello.path,'process')
    moose.reinit()
    moose.start(0.001)

def main():
    """
    This illustrates the use of rdesigneur to build a simple dendrite with
    spines, and then to resize them using spine fields. These are the
    fields that would be changed dynamically in a simulation with reactions
    that affect spine geometry.
    In this simulation there is a propagating reaction wave using a
    highly abstracted equation, whose product diffuses into the spines and
    makes them bigger.
    """
    #radCalc()
    #moose.delete('/model')
    print(f"[INFO ] Making model... ", end='')
    makeModel()
    
    for i in range( 11, 18 ): 
        moose.setClock( i, 0.0025 )
    moose.setClock( 18, 0.0025 )
    elec = moose.element( '/model/elec' )
    graphs = moose.Neutral( '/graphs' )
    Epsinit=moose.vec('/model/chem/dend/IRSpGr/Eps8')
    CaInit=moose.vec('/model/chem/dend/Ca')
    Cdcinit=moose.vec('/model/chem/dend/IRSpGr/Cdc42')
    Tiaminit=moose.vec('/model/chem/dend/Tiam1')
    IRSpinit=moose.vec('/model/chem/dend/IRSpGr/IRSp53')
    Cdcinit.concInit=0.0
    Tiaminit.concInit=0.0
    print(f"... done")

    elec = moose.element( '/model/elec' )


    print(f"[INFO ] Making plots ...", end='')
    
    makePlot( 'dend_curv_IRSp53', moose.vec( '/model/chem/dend/IRSpGr/curv_IRSp53' ), 'getN' )
    makePlot( 'sorting', moose.vec( '/model/chem/dend/IRSpGr/sort' ), 'getN' )
    makePlot( 'Ca', moose.vec( '/model/chem/dend/Ca' ), 'getN' )
    makePlot( 'CaM', moose.vec( '/model/chem/dend/CaM' ), 'getConc' )
    makePlot( 'CaM_GEF', moose.vec( '/model/chem/dend/Ras_gr/CaM_GEF' ), 'getConc' )
    makePlot( 'Ca4CaM', moose.vec( '/model/chem/dend/CaM_Ca4' ), 'getConc' )
    ###makePlot( 'RasGRF', moose.vec( '/model/chem/dend/Ras_gr/GEF_Gprot_bg' ), 'getConc' )
    makePlot( 'Cdc42', moose.vec( '/model/chem/dend/IRSpGr/Cdc42' ), 'getN' )
    makePlot( 'RacGTP', moose.vec( '/model/chem/dend/Rac_GTP' ), 'getN' )
    makePlot( 'Tiam', moose.vec( '/model/chem/dend/Tiam1' ), 'getN' )
    ##makePlot( 'spine_vasp', moose.vec( '/model/chem/spine/VASP' ), 'getN' )
    ##makePlot( 'spine_irsp53_vasp', moose.vec( '/model/chem/spine/IRSp53_VASP' ), 'getN' )
    #makePlot( 'head_Dia', eHead, 'getDiameter' )
    #makePlot( 'Shaft_Dia', eShaft, 'getDiameter' )
    rad=moose.element('/model/chem/dend/IRSpGr/Rad')
    mypyrun=moose.PyRun('/model/mypyrun')
    mypyrun.initString="""count=0"""
    mypyrun.runString="""count=count+1
radv=moose.vec('/model/chem/dend/IRSpGr/Rad').n
radm=radv[10]
output=radm
IRvec=moose.vec( '/model/chem/dend/IRSpGr/IRSp53' ).n
curv_IRSp53=moose.element( '/model/chem/dend/IRSpGr/curv_IRSp53')
Cdcvec=moose.vec( '/model/chem/dend/IRSpGr/Cdc42' ).n
if count==1:
    Cdcinit=moose.vec( '/model/chem/dend/IRSpGr/Cdc42' ).nInit
    pos=np.unravel_index(np.argmax(Cdcinit), Cdcinit.shape)
    pos=pos[0]
curv_IRSp53vec=moose.vec('/model/chem/dend/IRSpGr/curv_IRSp53').n
sigmaVec=moose.vec('/model/chem/dend/IRSpGr/sigma').n
IRSp53_mvec=moose.vec('/model/chem/dend/IRSpGr/IRSp53_m').n
Ipoolvec=moose.vec('/model/chem/dend/IRSpGr/Ipool').n
moose.vec('/model/chem/dend/IRSpGr/store_tube').n=moose.vec('/model/chem/dend/IRSpGr/IRSp53_temp').n
Kv=moose.vec('/model/chem/dend/IRSpGr/Kv').n
moose.vec('/model/chem/dend/IRSpGr/sort').n=(Ipoolvec-min(Ipoolvec))
sort_func=moose.vec('/model/chem/dend/IRSpGr/sort').n
for gri in range(0,len(Cdcvec)):
   sort_func[gri]=1.0*((3*float(sys.argv[2])*Kv[gri]*(1.0*curv_IRSp53vec[gri]+1.0*IRSp53_mvec[gri])**2)/(1.0+3.0*float(sys.argv[2])*Kv[gri]*(1.0*curv_IRSp53vec[gri]+1.0*IRSp53_mvec[gri])**2))*sort_func[gri]
if count==1:
   maxCIR=[]
   maxIR=[]
   max_curv=[]
#moose.vec('/model/chem/dend/IRSpGr/sigma').n=0.5-0.25*np.exp(-0.02*(count*0.0025))
for gri in range(0,len(Cdcvec)):
   sigmaVec[gri]=(0.3-0.25*np.exp(-0.015*count*0.0025))*((float(sys.argv[2])*3*curv_IRSp53vec[gri]**2)/(1+float(sys.argv[2])*3*curv_IRSp53vec[gri]**2))+0.0055
   s_m=1-np.exp(-sigmaVec[gri]/0.3)
   sort_func[gri]=sort_func[gri]-1.6*s_m*sort_func[gri]
moose.vec('/model/chem/dend/IRSpGr/sigma').n=sigmaVec   
moose.vec('/model/chem/dend/IRSpGr/sort_func').n=sort_func
moose.vec('/model/chem/dend/IRSpGr/detach').n=-moose.vec('/model/chem/dend/IRSpGr/sort_func').n+max(moose.vec('/model/chem/dend/IRSpGr/sort_func').n)
det_vec=moose.vec('/model/chem/dend/IRSpGr/detach').n
for gri in range(0,len(Cdcvec)):
   s_m=1-np.exp(-sigmaVec[gri]/0.3)
   det_vec[gri]=det_vec[gri]+1.6*s_m*det_vec[gri]
moose.vec('/model/chem/dend/IRSpGr/detach').n=det_vec   
temp_sort=moose.vec('/model/chem/dend/IRSpGr/sort_func').n

pref_curv=0.055
plt.xlabel('Number of IRSp53 dimers')
plt.ylabel('Deformation strength')
radmax=max(radv)
if count%100==0:
  print(" Mass conservation check  ")
  print("Sum of all forms of IRSp53 now")
  print(sum(moose.vec('/model/chem/dend/IRSpGr/IRSp53_dimer').n)+sum(moose.vec('/model/chem/dend/IRSpGr/IRSp53_a').n)+sum(moose.vec('/model/chem/dend/IRSpGr/IRSp53_m').n)+sum(moose.vec('/model/chem/dend/IRSpGr/curv_IRSp53').n),sum(moose.vec('/model/chem/dend/IRSpGr/IRSp53_m').n),'a',sum(moose.vec('/model/chem/dend/IRSpGr/IRSp53_a').n),sum(moose.vec('/model/chem/dend/IRSpGr/IRSp53_dimer').n),sum(moose.vec('/model/chem/dend/IRSpGr/curv_IRSp53').n))
  print("Initial sum")
  print(sum(moose.vec('/model/chem/dend/IRSpGr/IRSp53_dimer').nInit))
Rn=moose.element( '/model/chem/dend/IRSpGr/Rad' ).n
"""
    outputTab=moose.Table('/model/output')
    moose.connect(mypyrun,'output',outputTab,'input')
    moose.setClock(30, 0.0025)
    moose.setClock(10, 0.0025)
    moose.setClock(15, 0.0025)
    moose.setClock(16, 0.0025)
    mypyrun.mode=1
    print('... done')

    ###radCalc()
    ksolve = moose.wildcardFind('/##[TYPE=Ksolve]')[0]
    moose.reinit()
    test = 'False'
    step=0


    if test=='True':
       while step<int(runtime/0.001):
          I_M = moose.element('/model/chem/dend/IRSpGr/IRSp53_m').n
          I_C = moose.element('/model/chem/dend/IRSpGr/IRSp53').n
          Rad = moose.element('/model/chem/dend/IRSpGr/Rad').n
          gradBAR = moose.element('/model/chem/dend/IRSpGr/gradBAR').expr
          Radfun = moose.element('/model/chem/dend/IRSpGr/Radfun').expr
          recBAR = moose.element('/model/chem/dend/IRSpGr/recBAR').expr
          gradBAReval = 2.1*1e-28*(I_M-I_C)*(1/(0.2*0.2*1e-12))*I_C+0*Rad
          if I_P > 0:
            Radfuneval=np.sqrt((55*4*1e-18)/(2*(0.02-0.08*(np.log(1/(I_P*50*100+1))))))
          raw_input()
          moose.start(60)
          step=step+1

    test_prop='True'
    if test_prop=='True':
      print('\n TEST PROPAGATING WAVES \n')
      Rac_m_d=[]
      tube_GTP=[]
      Ras_d=[]
      Kv_d=[]
      Rad_d=[]
      tube_IR=[]
      store_tube_d=[]
      Tiam_inact_d=[]
      #CaM_GEF_d=[]
      sigma_d=[]
      Eps8_d=[]
      Rac_GTP_d=[]
      Rac_GDP_d=[]
      Cdc42_GDP_d=[]
      Tiam1_d=[]
      Cdc42_d=[]
      CaMKII_thr286_d=[]
      Ca_d=[]
      curv_IRSp53_d=[]
      recavec=[]
      sort_d=[]
      rec = []
      conv_d=[]
      IM_d=[]
      Cdc_m_d=[]
      IR_act=[]
      loc1_vox=(int(loc1)*dendLen)/50e-9
      loc2_vox=(int(loc2)*dendLen)/50e-9
      loc3_vox=(int(loc3)*dendLen)/50e-9
      
      pert_int(int(loc1_vox),int(sys.argv[11]))
      temp=moose.vec('/model/chem/dend/IRSpGr/rec').n
      recavec.append(moose.vec('/model/chem/dend/IRSpGr/rec').n)
      gap=int(loc2_vox)-int(loc1_vox)
      gap_p=int(loc3_vox)-int(loc1_vox)
      recavec.append([0.0+float(sys.argv[12])*temp[i-gap_p] for i in range(0,len(moose.vec('/model/chem/dend/IRSpGr/rec').n))])
      double='True'
      delay='True'
      simult='True'
      if double=='True':
        if delay=='True':
           moose.vec('/model/chem/dend/IRSpGr/rec').n=[temp[i-gap] for i in range(0,len(moose.vec('/model/chem/dend/IRSpGr/rec').n))]
           recavec.append(moose.vec('/model/chem/dend/IRSpGr/rec').n)
      if simult=='True':
          moose.vec('/model/chem/dend/IRSpGr/rec').n=[temp[i-gap] for i in range(0,len(moose.vec('/model/chem/dend/IRSpGr/rec').n))]
      if simult=='True':
          moose.vec('/model/chem/dend/IRSpGr/rec').n=moose.vec('/model/chem/dend/IRSpGr/rec').n+temp
          recavec.append(moose.vec('/model/chem/dend/IRSpGr/rec').n)
      if float(sys.argv[15])==0.0:
          moose.vec('/model/chem/dend/IRSpGr/rec').n=recavec[3]
      else:
          moose.vec('/model/chem/dend/IRSpGr/rec').n=recavec[0]
      for runt in range(runtime):
        moose.start(0.5)
        time_now=runt*0.5
        print('TIME', time_now)
        if double=='True':
          if delay=='True':
            if float(sys.argv[15])<=60.0:
              if time_now>=(float(sys.argv[15])) and time_now<=60.0:
                 print('simult')
                 moose.vec('/model/chem/dend/IRSpGr/rec').n=recavec[3]
              elif time_now>60.0:
                 moose.vec('/model/chem/dend/IRSpGr/rec').n=recavec[2]
            else:
              if time_now>=(float(sys.argv[15])):
                print('double pulse')
                moose.vec('/model/chem/dend/IRSpGr/rec').n=recavec[2]
        Ras_d.append(moose.vec('/model/chem/dend/Ras_gr/GTP_Ras').n)
        Tiam_inact_d.append(moose.vec('/model/chem/dend/Tiam_inact').n)
        Rad_d.append(moose.vec('/model/chem/dend/IRSpGr/Rad').n)
        Cdc42_GDP_d.append(moose.vec('/model/chem/dend/Cdc42_GDP').n)
        Eps8_d.append(moose.vec('/model/chem/dend/IRSpGr/Eps8').n)
        sort_d.append(moose.vec('/model/chem/dend/IRSpGr/sort_func').n)
        Kv_d.append(moose.vec('/model/chem/dend/IRSpGr/Kv').n)
        Rac_GDP_d.append(moose.vec('/model/chem/dend/Rac_GDP').n)
        conv_d.append(moose.vec('/model/chem/dend/conv').n)
        #CaM_GEF_d.append(moose.vec('/model/chem/dend/Ras_gr/CaM_GEF').n)
        sigma_d.append(moose.vec('/model/chem/dend/IRSpGr/sigma').n)
        Rac_GTP_d.append(moose.vec('/model/chem/dend/Rac_GTP').n)
        Tiam1_d.append(moose.vec('/model/chem/dend/Tiam1').n)
        Cdc42_d.append(moose.vec('/model/chem/dend/IRSpGr/Cdc42').n)
        CaMKII_thr286_d.append(moose.vec('/model/chem/dend/CaMKII_gr/CaMKII_thr286').n)
        #Ca_d.append(moose.vec('/model/chem/dend/Ca').n)
        curv_IRSp53_d.append(moose.vec('/model/chem/dend/IRSpGr/curv_IRSp53').n)
        tube_IR.append(moose.vec('/model/chem/dend/IRSpGr/tube_IRSp53').n)
        IR_act.append(moose.vec('/model/chem/dend/IRSpGr/IR_actin').n)
        IM_d.append(moose.vec('/model/chem/dend/IRSpGr/IRSp53_m').n)
        Cdc_m_d.append(moose.vec('/model/chem/dend/IRSpGr/Cdc42_m').n)
        Rac_m_d.append(moose.vec('/model/chem/dend/IRSpGr/Rac_m').n)
        rec.append(moose.vec('/model/chem/dend/IRSpGr/rec').n)
        #Metab_d.append(moose.vec('/model/chem/dend/IRSpGr/Metabolism').n)
      list_td = [Rac_GTP_d,Rad_d,IR_act,sigma_d,Rac_GTP_d,Tiam1_d,Cdc42_d,tube_IR,curv_IRSp53_d,CaMKII_thr286_d,sort_d,rec]
      file_list=['Rac_GTP'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'F'+str(sys.argv[13])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','Rad'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'F'+str(sys.argv[13])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','IR_act'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'F'+str(sys.argv[13])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','sigma_d_'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'F'+str(sys.argv[13])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','Rac_GTP_d_'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'F'+str(sys.argv[13])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','Tiam1_d_'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'F'+str(sys.argv[13])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','Cdc42_d_'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'F'+str(sys.argv[13])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','tube_IR'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'F'+str(sys.argv[13])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','diffStudy'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'F'+str(sys.argv[13])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','CaMKII'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','sort_d'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml','rec'+str(sys.argv[2])+'S'+str(sys.argv[11])+'STR'+str(sys.argv[12])+'C'+str(sys.argv[14])+'dt'+str(sys.argv[15])+'t'+str(sys.argv[9])+'d'+str(sys.argv[10])+'l1'+str(sys.argv[6])+'l2'+str(sys.argv[7])+'l3'+str(sys.argv[8])+'Diff'+str(sys.argv[17])+'.xml']
      print('WRITING TIME DATA FOR OTHERS')
      cf=0
      for wi in list_td:
         fileName_td=file_list[cf]
         writeXML_td(wi,fileName_td)
         cf=cf+1
    test_pop=1
    
if __name__ == '__main__':
   main()
